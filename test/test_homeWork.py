from selenium import webdriver
from selenium.webdriver.common.by import By

def test_paragraph():
    server = "http://selenium__standalone-chrome:4444/wd/hub"
    options = webdriver.ChromeOptions()
    driver = webdriver.Remote(command_executor=server, options=options)

    driver.get("https://the-internet.herokuapp.com/context_menu")
    paragraphs = driver.find_elements(By.TAG_NAME, "p")
    assert "Right-click in the box below to see one called 'the-internet'" in paragraphs[1].text

    driver.quit()


def test_alibaba():
    server = "http://selenium__standalone-chrome:4444/wd/hub"
    options = webdriver.ChromeOptions()
    driver = webdriver.Remote(command_executor=server, options=options)

    driver.get("https://the-internet.herokuapp.com/context_menu")

    temp = []
    all_elements = driver.find_elements(By.XPATH, ".//*")
    for element in all_elements:
        temp.append(element.text)
    assert any("Alibaba" in sub for sub in temp)

    driver.quit()

    